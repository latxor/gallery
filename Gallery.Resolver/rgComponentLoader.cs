﻿
using System;  
using System.Collections.Generic;  
using System.Linq;  
using System.Text;  
using Microsoft.Practices.Unity;  
using System.ComponentModel.Composition.Hosting;  
using System.ComponentModel.Composition.Primitives;  
using System.Reflection;  
  
namespace Gallery.Resolver
{  
    public static class rgComponentLoader  
    {  
        public static void LoadContainer(IUnityContainer container, string path, string pattern)  
        {  
            var dirCat = new DirectoryCatalog(path, pattern);  
            var importDef = BuildImportDefinition();  
            try  
            {  
                using (var aggregateCatalog = new AggregateCatalog())  
                {  
                    aggregateCatalog.Catalogs.Add(dirCat);  
  
                    using (var componsitionContainer = new CompositionContainer(aggregateCatalog))  
                    {  
                        IEnumerable<Export> exports = componsitionContainer.GetExports(importDef);  
  
                        IEnumerable<rgIComponent> modules =  
                            exports.Select(export => export.Value as rgIComponent).Where(m => m != null);  
  
                        var registerComponent = new rgRegisterComponent(container);  
                        foreach (rgIComponent module in modules)  
                        {  
                            module.SetUp(registerComponent);  
                        }  
                    }  
                }  
            }  
            catch (ReflectionTypeLoadException typeLoadException)  
            {  
                var builder = new StringBuilder();  
                foreach (Exception loaderException in typeLoadException.LoaderExceptions)  
                {  
                    builder.AppendFormat("{0}\n", loaderException.Message);  
                }  
  
                throw new TypeLoadException(builder.ToString(), typeLoadException);  
            }  
        }  
  
        private static ImportDefinition BuildImportDefinition()  
        {  
            return new ImportDefinition(def => true, typeof(rgIComponent).FullName, ImportCardinality.ZeroOrMore, false, false);  
        }  
    }  
  
    
}  