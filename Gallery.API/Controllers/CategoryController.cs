﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Gallery.Bussines.Factory;
using Gallery.Data.Model;
using Rt.Core.Bussines.Factory;

namespace Gallery.API.Controllers
{
    [Authorize]
    public class CategoryController : ApiController
    {
        [HttpGet]
        [Route("api/Category/GetAll")]
        public IHttpActionResult Get()
        {
            int userid = User.Identity.GetUserId<int>();
            var Categories = rtServicesFactory.CategoryServices().GetAll(userid);
            return Ok(Categories);            
        }

        [HttpPost]
        [Route("api/Category/Create")]
        public IHttpActionResult Create([FromBody]string name)
        {
            int userid = User.Identity.GetUserId<int>();
            if (!string.IsNullOrWhiteSpace(name))
            {
                var listcategoriesselected = rtServicesFactory.CategoryServices().GetAll(userid).Where(c=>c.Selected== true);
                Category category = new Category() { Name = name, Enable = true, UserId = userid,
                CleanName = UrlFactory.RemoveSpecialCharacters(name) };

                if (listcategoriesselected.Count() <= 0)
                {
                    category.Selected = true;
                }

                var result = rtServicesFactory.CategoryServices().Create(category, userid);
                if (result > 0)
                    return Ok(true);
                else
                    return BadRequest("No se pudo guardar la nueva categoria, Intentelo de nuevo.");

            }
                return BadRequest("Nombre no puede ser vacio, Por favor coloquele un nombre");
        }

        [HttpGet]
        [Route("api/Category/FindCategory")]
        public IHttpActionResult FindCategory(string name)
        {
            int userid = User.Identity.GetUserId<int>();
            if (string.IsNullOrEmpty(name))
            {
                var allcategories = rtServicesFactory.CategoryServices().GetAll(userid);
                return Ok(allcategories);
            }

            var categories = rtServicesFactory.CategoryServices().GetItemByName(name, userid);
            return Ok(categories);
        }

        [HttpGet]
        [Route("api/Category/FindCategoryById")]
        public IHttpActionResult FindCategoryById(int id)
        {
            int userid = User.Identity.GetUserId<int>();
            var category = rtServicesFactory.CategoryServices().GetItemById(id, userid);
            return Ok(category);
        }

        [HttpPost]
        [Route("api/Category/AddImageToCategory")]
        public IHttpActionResult AddImageToCategory([FromBody]int categorySelectd, [FromBody]int typeOption, [FromBody]string url)
        {            
            int result = -1;
            switch (typeOption)
            {
                case 1:
                    GfyCat NewGfyCat = new GfyCat() { CategoryId = categorySelectd, Url = url, CleanUrl = url };
                    result = rtServicesFactory.GfyCatServices().Create(NewGfyCat, 1);
                    break;
                case 2:
                    Gifv NewGifv = new Gifv() { CategoryId = categorySelectd, Url = url, CleanUrl = url };
                    result = rtServicesFactory.GifvServices().Create(NewGifv, 1);
                    break;
                case 3:
                    Gif NewGif = new Gif() { CategoryId = categorySelectd, Url = url, CleanUrl = UrlFactory.RemoveSpecialCharacters(url) };
                    result = rtServicesFactory.GifServices().Create(NewGif, 1);
                    break;
                case 4:
                    OthersImages NewOthersImages = new OthersImages() { CategoryId = categorySelectd, Url = url, CleanUrl = UrlFactory.RemoveSpecialCharacters(url) };
                    result = rtServicesFactory.OtherImagesServices().Create(NewOthersImages, 1);
                    break;

                default:
                    break;
            }

            if (result > 0)
                return Ok(true);
            else
                return BadRequest();


        }
    }
}
