﻿using Gallery.Bussines.Services;
using Gallery.Data.Context;
using Gallery.Data.UnitOfWork;
using Rt.Core.Bussines.Services;
using Rt.Core.Data.Model;
using Rt.Core.Data.UnitOfWork;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Gallery.Bussines.Factory
{
    public static class rtServicesFactory
    {


        public static CategoryServices CategoryServices()
        {
            return new CategoryServices(new rtUnitOfWork());
        }

        public static GfyCatServices GfyCatServices()
        {
            return new GfyCatServices(new rtUnitOfWork());
        }

        public static GifvServices GifvServices()
        {
            return new GifvServices(new rtUnitOfWork());
        }

        public static GifServices GifServices()
        {
            return new GifServices(new rtUnitOfWork());
        }

        public static OtherImagesServices OtherImagesServices()
        {
            return new OtherImagesServices(new rtUnitOfWork());
        }



        //public static GroupServices GroupServices()
        //{
        //    return new GroupServices(new rtUnitOfWork());
        //}



        public static rtUserServices UserServices()
        {
            return new rtUserServices(new rtUserRepository(new GalleryContext("Default")));
        }

  

    }
}
