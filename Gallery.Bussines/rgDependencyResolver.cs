﻿using Gallery.Bussines.Services;
using Microsoft.AspNet.Identity;
using Microsoft.Practices.Unity;

using Rt.Core.Data.Model;
using Rt.Core.Data.UnitOfWork;
using Rt.Core.Resolver;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Bussines.Bussines
{
    [Export(typeof(rtIComponent))]
    public class rgDependencyResolver : rtIComponent
    {
        public void SetUp(rtIRegisterComponent registerComponent)
        {
            
            registerComponent.RegisterType<IUserStore<rtUser, int>, rtUserRepository>();

            
            registerComponent.RegisterType<ICategoryServices, CategoryServices>();
            registerComponent.RegisterType<IGfyCatServices, GfyCatServices>();
            
            




        }
    }
}
