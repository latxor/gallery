﻿using Gallery.Bussines.Repository;
using Gallery.Data.Model;
using Gallery.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Gallery.Bussines.Services
{
    public class GfyCatServices : IGfyCatServices
    {

        private readonly rtUnitOfWork _unitOfWork;

        public GfyCatServices(rtUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public int Create(GfyCat modelApp)
        {
            throw new NotImplementedException();
        }



        public int Create(GfyCat modelApp, int userId)
        {
            var response = GfyCatRepository.Get(modelApp.Url);

            try
            {
           
                if (response.gfyItem != null)
                {
                    using (var scope = new TransactionScope())
                    {
                        //Aca ya debe estar incluido el id de la empresa    
                        modelApp.gfyItem = response.gfyItem;
                        modelApp.Enable = true;
                        //rtModelAuditoryHelper.SetCreate(ref modelApp, userId);
                        _unitOfWork.GfyCatRepository.Insert(modelApp);
                        _unitOfWork.Save();
                        scope.Complete();

                        return modelApp.Id;
                    }
                }
                    return -1;
            }
            catch (Exception ex)
            {

                return -1;
            }
           

        }

        public bool Delete(int modelId)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int modelId, int userId)
        {
            var success = false;
            if (modelId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var modelDb = _unitOfWork.GfyCatRepository.GetByID(modelId);
                    if (modelDb != null)
                    {
                        modelDb.Enable = false;
                        //rtModelAuditoryHelper.SetDelete(ref modelDb, userId);

                        _unitOfWork.GfyCatRepository.Update(modelDb);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public IEnumerable<GfyCat> GetAll()
        {
            var categories = _unitOfWork.GfyCatRepository.GetAll();
            if (categories != null)
                return categories;

            return null;
        }

        public GfyCat GetItemById(int modelId)
        {
            GfyCat GfyCat = _unitOfWork.GfyCatRepository.GetByID(modelId);
            if (GfyCat != null)
                return GfyCat;

            return null;
        }


        public bool Update(GfyCat modelApp)
        {
            throw new NotImplementedException();
        }

        public bool Update(GfyCat modelApp, int userId)
        {
            var success = false;
            if (modelApp != null)
            {
                using (var scope = new TransactionScope())
                {
                    var modelDb = _unitOfWork.GfyCatRepository.GetByID(modelApp.Id);
                    if (modelDb != null)
                    {
                        modelDb.CategoryId = modelApp.CategoryId;


                        //rtModelAuditoryHelper.SetUpdate(ref modelDb, userId);

                        _unitOfWork.GfyCatRepository.Update(modelDb);

                        _unitOfWork.Save();

                        scope.Complete();

                        success = true;
                    }
                }
            }
            return success;
        }
    }
}
