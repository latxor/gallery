﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gallery.Data.Model;
using System.Transactions;
using Gallery.Data.UnitOfWork;
using Rt.Core.Helper;

namespace Gallery.Bussines.Services
{
    public class CategoryServices : ICategoryServices
    {
        private readonly static string[] includes = { "GfyCat.gfyItem", "Gifv.gifvitem", "Gif", "OthersImages" };
        private readonly rtUnitOfWork _unitOfWork;

        public CategoryServices(rtUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public int Create(Category modelApp)
        {
            throw new NotImplementedException();
        }

        

        public int Create(Category modelApp, int userId)
        {
            
            using (var scope = new TransactionScope())
            {
                //Aca ya debe estar incluido el id de la empresa    
                modelApp.Selected = false;                   
                modelApp.Enable = true;

                //rtModelAuditoryHelper.SetCreate(ref modelApp, userId);
                _unitOfWork.CategoryRepository.Insert(modelApp);
                _unitOfWork.Save();
                scope.Complete();

                return modelApp.Id;
            }
        }

        public bool Delete(int modelId)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int modelId, int userId)
        {
            var success = false;
            if (modelId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var modelDb = _unitOfWork.CategoryRepository.GetByID(modelId);
                    if (modelDb != null)
                    {
                        modelDb.Enable = false;
                        //rtModelAuditoryHelper.SetDelete(ref modelDb, userId);

                        _unitOfWork.CategoryRepository.Update(modelDb);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public IEnumerable<Category> GetAll()
        {
            var categories = _unitOfWork.CategoryRepository.GetAll().Where(c=>c.Enable== true).ToList();
            if (categories!=null)            
                return categories;
            
            return null;
        }

        public IEnumerable<Category> GetAll(int userid)
        {
            var categories = _unitOfWork.CategoryRepository.GetAll().Where(c => c.Enable == true && c.UserId==userid).ToList();
            if (categories != null)
                return categories;

            return null;
        }

        public Category GetItemById(int modelId)
        {
            throw new NotImplementedException();
        }

        public Category GetItemById(int modelId,int userid)
        {
            //Category category = _unitOfWork.CategoryRepository.GetByID(modelId);
            Category category = _unitOfWork.CategoryRepository.GetWithInclude(c => c.Enable == true && c.Id == modelId && c.UserId==userid, includes).FirstOrDefault();
            if (category!=null)            
                return category;
            
            return null;
        }

        public List<Category> GetItemByName(string name)
        {
            var category = _unitOfWork.CategoryRepository.GetMany(c => c.Enable == true && c.Name.Contains(name)).ToList();
            if (category != null)
                return category;

            return null;
        }

        public List<Category> GetItemByName(string name,int userid)
        {
            var category = _unitOfWork.CategoryRepository.GetMany(c => c.Enable == true && c.Name.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0 && c.UserId==userid).ToList();
            if (category != null)
                return category;

            return null;
        }

        public List<Category> GetItemBySelected(bool selected, int userid)
        {
           
            var category = _unitOfWork.CategoryRepository.GetWithInclude(c => c.Enable == true && c.Selected == selected && c.UserId==userid, includes).ToList();


            if (category != null)
                return category;

            return null;
        }

        public Category GetCategoryBySelected(bool selected, int userid)
        {

            var category = _unitOfWork.CategoryRepository.GetWithInclude(c => c.Enable == true && c.Selected == selected && c.UserId == userid, includes).FirstOrDefault();

            return category;
        }

        public List<Category> GetItemBySelected(bool selected)
        {

            var category = _unitOfWork.CategoryRepository.GetWithInclude(c => c.Enable == true && c.Selected == selected, includes).ToList();


            if (category != null)
                return category;

            return null;
        }

        public bool Update(Category modelApp)
        {
            throw new NotImplementedException();
        }

        public bool Update(Category modelApp, int userId)
        {
            var success = false;
            if (modelApp != null)
            {
                using (var scope = new TransactionScope())
                {
                    var modelDb = _unitOfWork.CategoryRepository.GetByID(modelApp.Id);
                    if (modelDb != null)
                    {
                        modelDb.Name = modelApp.Name;
                        modelDb.Selected = modelApp.Selected;

                        modelDb.UserId = modelApp.UserId;
                        modelDb.CleanName = modelApp.CleanName;
                        

                        modelDb.GfyCatPanel = modelApp.GfyCatPanel;
                        modelDb.GifvPanel = modelApp.GifvPanel;
                        modelDb.GifPanel = modelApp.GifPanel;
                        modelDb.OtherImagePanel = modelApp.OtherImagePanel;

                        //rtModelAuditoryHelper.SetUpdate(ref modelDb, userId);

                        _unitOfWork.CategoryRepository.Update(modelDb);

                        _unitOfWork.Save();

                        scope.Complete();

                        success = true;
                    }
                }
            }
            return success;
        }
    }
}
