﻿using Gallery.Data.Model;
using Gallery.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Gallery.Bussines.Services
{
    public class OtherImagesServices : IOtherImagesServices
    {
        private readonly rtUnitOfWork _unitOfWork;
        public OtherImagesServices(rtUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public int Create(OthersImages modelApp)
        {
            throw new NotImplementedException();
        }



        public int Create(OthersImages modelApp, int userId)
        {

            using (var scope = new TransactionScope())
            {
                //Aca ya debe estar incluido el id de la empresa    
                
                modelApp.Enable = true;

                //rtModelAuditoryHelper.SetCreate(ref modelApp, userId);
                _unitOfWork.OthersImagesRepository.Insert(modelApp);
                _unitOfWork.Save();
                scope.Complete();

                return modelApp.Id;
            }
        }

        public bool Delete(int modelId)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int modelId, int userId)
        {
            var success = false;
            if (modelId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var modelDb = _unitOfWork.OthersImagesRepository.GetByID(modelId);
                    if (modelDb != null)
                    {
                        modelDb.Enable = false;
                        //rtModelAuditoryHelper.SetDelete(ref modelDb, userId);

                        _unitOfWork.OthersImagesRepository.Update(modelDb);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public IEnumerable<OthersImages> GetAll()
        {
            var categories = _unitOfWork.OthersImagesRepository.GetAll().Where(c => c.Enable == true).ToList();
            if (categories != null)
                return categories;

            return null;
        }

        public OthersImages GetItemById(int modelId)
        {
            //OthersImages OthersImages = _unitOfWork.OthersImagesRepository.GetByID(modelId);
            OthersImages OthersImages = _unitOfWork.OthersImagesRepository.GetByID(modelId);
            if (OthersImages != null)
                return OthersImages;

            return null;
        }

        

        

        public bool Update(OthersImages modelApp)
        {
            throw new NotImplementedException();
        }

        public bool Update(OthersImages modelApp, int userId)
        {
            var success = false;
            if (modelApp != null)
            {
                using (var scope = new TransactionScope())
                {
                    var modelDb = _unitOfWork.OthersImagesRepository.GetByID(modelApp.Id);
                    if (modelDb != null)
                    {
                        modelDb.CategoryId = modelApp.CategoryId;
                        

                        //rtModelAuditoryHelper.SetUpdate(ref modelDb, userId);

                        _unitOfWork.OthersImagesRepository.Update(modelDb);

                        _unitOfWork.Save();

                        scope.Complete();

                        success = true;
                    }
                }
            }
            return success;
        }
    }
}
