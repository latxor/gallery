﻿using Gallery.Bussines.Repository;
using Gallery.Data.Model;
using Gallery.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Gallery.Bussines.Services
{
    public class GifServices: IGifInterface
    {
        private readonly rtUnitOfWork _unitOfWork;

        public GifServices(rtUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public int Create(Gif modelApp)
        {
            throw new NotImplementedException();
        }



        public int Create(Gif modelApp, int userId)
        {
            try
            {
                byte[] response = GifRepository.Get(modelApp.Url);

                if (response != null)
                {
                    using (var scope = new TransactionScope())
                    {
                        //Aca ya debe estar incluido el id de la empresa    
                        modelApp.FirstFrame = response;
                        modelApp.Enable = true;

                        //rtModelAuditoryHelper.SetCreate(ref modelApp, userId);
                        _unitOfWork.GifRepository.Insert(modelApp);
                        _unitOfWork.Save();
                        scope.Complete();

                        return modelApp.Id;
                    }
                }
                return -1;
            }
            catch (Exception)
            {

                return -1;
            }


        }

        public bool Delete(int modelId)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int modelId, int userId)
        {
            var success = false;
            if (modelId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var modelDb = _unitOfWork.GifRepository.GetByID(modelId);
                    if (modelDb != null)
                    {
                        modelDb.Enable = false;
                        //rtModelAuditoryHelper.SetDelete(ref modelDb, userId);

                        _unitOfWork.GifRepository.Update(modelDb);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public IEnumerable<Gif> GetAll()
        {
            var categories = _unitOfWork.GifRepository.GetAll();
            if (categories != null)
                return categories;

            return null;
        }

        public Gif GetItemById(int modelId)
        {
            Gif Gif = _unitOfWork.GifRepository.GetByID(modelId);
            if (Gif != null)
                return Gif;

            return null;
        }


        public bool Update(Gif modelApp)
        {
            throw new NotImplementedException();
        }

        public bool Update(Gif modelApp, int userId)
        {
            var success = false;
            if (modelApp != null)
            {
                using (var scope = new TransactionScope())
                {
                    var modelDb = _unitOfWork.GifRepository.GetByID(modelApp.Id);
                    if (modelDb != null)
                    {
                        modelDb.CategoryId = modelApp.CategoryId;


                        //rtModelAuditoryHelper.SetUpdate(ref modelDb, userId);

                        _unitOfWork.GifRepository.Update(modelDb);

                        _unitOfWork.Save();

                        scope.Complete();

                        success = true;
                    }
                }
            }
            return success;
        }
    }
}
