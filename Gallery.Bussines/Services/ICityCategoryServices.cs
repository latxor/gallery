﻿using Rt.Core.Bussines;
using Gallery.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Bussines.Services
{
    public interface ICityCategory<TModel> where TModel : class

    {
        int Create(TModel modelApp);
        int Create(TModel modelApp, int userId);
        bool Delete(int modelId);
        bool Delete(int modelId, int userId);
        IEnumerable<TModel> GetAll();
        TModel GetItemById(int modelId);
        bool Update(TModel modelApp);
        bool Update(TModel modelApp, int userId);
    }
}
