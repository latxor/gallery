﻿using Gallery.Bussines.Repository;
using Gallery.Data.Model;
using Gallery.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Gallery.Bussines.Services
{
    public class GifvServices:IGifvServices
    {
        private readonly rtUnitOfWork _unitOfWork;

        public GifvServices(rtUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public int Create(Gifv modelApp)
        {
            throw new NotImplementedException();
        }



        public int Create(Gifv modelApp, int userId)
        {
            try
            {
                var response = GifvRepository.Get(modelApp.Url);
                bool respose = response!=null? true:false;
                bool respose1 = !string.IsNullOrEmpty(response.mp4) ? true : false;
                bool respose2 = !string.IsNullOrEmpty(response.webm) ? true : false;

                if (response != null)
                {


                    using (var scope = new TransactionScope())
                    {
                        //Aca ya debe estar incluido el id de la empresa    
                        modelApp.gifvitem = response;
                        modelApp.Enable = true;
                        
                        //rtModelAuditoryHelper.SetCreate(ref modelApp, userId);
                        _unitOfWork.GifvRepository.Insert(modelApp);
                        _unitOfWork.Save();
                        scope.Complete();

                        return modelApp.Id;
                    }
                }
                return -1;
            }
            catch (Exception)
            {

                return -1;
            }


        }

        public bool Delete(int modelId)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int modelId, int userId)
        {
            var success = false;
            if (modelId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var modelDb = _unitOfWork.GifvRepository.GetByID(modelId);
                    if (modelDb != null)
                    {
                        modelDb.Enable = false;
                        //rtModelAuditoryHelper.SetDelete(ref modelDb, userId);

                        _unitOfWork.GifvRepository.Update(modelDb);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public IEnumerable<Gifv> GetAll()
        {
            var categories = _unitOfWork.GifvRepository.GetAll();
            if (categories != null)
                return categories;

            return null;
        }

        public Gifv GetItemById(int modelId)
        {
            Gifv Gifv = _unitOfWork.GifvRepository.GetByID(modelId);
            if (Gifv != null)
                return Gifv;

            return null;
        }


        public bool Update(Gifv modelApp)
        {
            throw new NotImplementedException();
        }

        public bool Update(Gifv modelApp, int userId)
        {
            var success = false;
            if (modelApp != null)
            {
                using (var scope = new TransactionScope())
                {
                    var modelDb = _unitOfWork.GifvRepository.GetByID(modelApp.Id);
                    if (modelDb != null)
                    {
                        modelDb.CategoryId = modelApp.CategoryId;


                        //rtModelAuditoryHelper.SetUpdate(ref modelDb, userId);

                        _unitOfWork.GifvRepository.Update(modelDb);

                        _unitOfWork.Save();

                        scope.Complete();

                        success = true;
                    }
                }
            }
            return success;
        }
    }
}
