﻿using Gallery.Data.Model;
using Imgur.API;
using Imgur.API.Authentication.Impl;
using Imgur.API.Endpoints.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Bussines.Repository
{
    public static class GifvRepository
    {
        private readonly static string id = "ebeca62a0ed84f0";
        private readonly static string token = "67f9c60f31452780fc4c582109ab5580416c2447";

        public static gifvitem Get(string url)
        {
            try
            {
                var client = new ImgurClient(id, token);
                var endpoint = new ImageEndpoint(client);                
                var task = endpoint.GetImageAsync(url);
                Task.WaitAll(task);
                var image = task.Result;

                gifvitem gifvitem = new gifvitem(image.Title, image.Link, image.Name, image.Gifv, image.Mp4, image.Webm);

                return gifvitem;
            }
            catch (ImgurException imgurEx)
            {
                return null;
            }
            
        }
    }
}
