﻿using ImageMagick;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Bussines.Repository
{
    public static class GifRepository
    {
        public static byte[] Get(string url)
        {
            var webClient = new WebClient();            
            byte[] imageBytes = webClient.DownloadData(url);

            MagickReadSettings settings = new MagickReadSettings();            
            settings.Width = 100;
            settings.Height = 100;
            using (MemoryStream memStream = new MemoryStream())
            {
                using (MagickImage image = new MagickImage(imageBytes, settings))
                {
                    image.Write(memStream);
                }
                return memStream.ToArray();
            }

        }
    }
}
