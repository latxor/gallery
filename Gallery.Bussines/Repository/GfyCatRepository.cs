﻿using Gallery.Data.Model;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Bussines.Repository
{
    public static class GfyCatRepository
    {
        public static GfyCat Get(string id)
        {

            var client = new RestClient("https://gfycat.com");
            var request = new RestRequest("cajax/get/{id}", Method.POST);
            request.AddUrlSegment("id", id);

            IRestResponse response = client.Execute(request);
            string content = response.Content;

            IRestResponse DownloadResponse = client.Execute(request);
            if (DownloadResponse.ResponseStatus == ResponseStatus.Completed)
            {
                var gfycat = JsonConvert.DeserializeObject<GfyCat>(DownloadResponse.Content);
                if (gfycat.gfyItem!=null)
                {
                    return gfycat;
                }
            }
            return null;
 
        }
    }
}
