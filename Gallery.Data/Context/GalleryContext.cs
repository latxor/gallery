﻿using Gallery.Data.Map;
using Gallery.Data.Model;
using Rt.Core.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gallery.Data.Migrations;
using Rt.Core.Data.Model;

namespace Gallery.Data.Context
{
    public class GalleryContext : rtContextBase
    {
        private static string conection1 = string.Format(@"Server={0};  Database={1};User Id={2};  Password={3};", @".\DESARROLLO", "dbGallery", "sa", "sapassword");
        private static string conection2 = "Data Source=SQL5019.Smarterasp.net;Initial Catalog=DB_9DA279_Galeria;User Id=DB_9DA279_Galeria_admin;Password=Abc_12345;";

        //#if true
        public GalleryContext() : base(conection2)
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<GalleryContext, Configuration>(conection1));
        }

        public GalleryContext(string conection) : base(conection2)
        { }


        ////#else
        //public GalleryContext() : base(conection2)
        //{
        //    Database.SetInitializer(new MigrateDatabaseToLatestVersion<GalleryContext, Configuration>(conection2));
        //}

        //public GalleryContext(string conection) : base(conection2)
        //{ }
        ////#endif


        public DbSet<Category> Category { get; set; }
        public DbSet<GfyCat> GfyCat { get; set; }
        public DbSet<Gifv> Gifv { get; set; }
        public DbSet<Gif> Gif { get; set; }
        public DbSet<OthersImages> OthersImages { get; set; }
        public DbSet<gfyItem> gfyItem { get; set; }
        public DbSet<gifvitem> gifvitem { get; set; }
        





        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            
            modelBuilder.Configurations.Add(new CategoryMap());
            modelBuilder.Configurations.Add(new GfyCatMap());
            modelBuilder.Configurations.Add(new GifvMap());
            modelBuilder.Configurations.Add(new GifMap());
            modelBuilder.Configurations.Add(new OtherImagesMap());


            modelBuilder.Entity<GfyCat>()
            .HasRequired(c1 => c1.gfyItem)
            .WithRequiredPrincipal(c2 => c2.GfyCat);

            modelBuilder.Entity<Gifv>()
           .HasRequired(c1 => c1.gifvitem)
           .WithRequiredPrincipal(c2 => c2.Gifv);

            //MUCHOS A MUCHOS
            //modelBuilder.Entity<CityCategory>().HasKey(c => new { c.CityId, c.CategoryId });


            base.OnModelCreating(modelBuilder);
        }

    }
}
