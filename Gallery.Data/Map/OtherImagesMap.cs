﻿using Gallery.Data.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Data.Map
{
    public class OtherImagesMap : EntityTypeConfiguration<OthersImages>
    {
        public OtherImagesMap()
        {
            ToTable("OtherImages");

            HasKey(t => t.Id);
            Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.Enable).HasColumnName("Enable").IsRequired();
            Property(t => t.Url).HasColumnName("Url").IsRequired();
            Property(t => t.CleanUrl).HasColumnName("CleanUrl").IsOptional();

            Property(t => t.CategoryId).HasColumnName("CategoryId").IsRequired();




        }
    }
}
