﻿using Gallery.Data.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Data.Map
{
    public class GifMap:EntityTypeConfiguration<Gif>
    {
        public GifMap()
        {
            ToTable("Gif");

            HasKey(t => t.Id);
            Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.Enable).HasColumnName("Enable").IsRequired();
            Property(t => t.Url).HasColumnName("Url").IsRequired();
            Property(t => t.CleanUrl).HasColumnName("CleanUrl").IsOptional();

            Property(t => t.FirstFrame).HasColumnName("FirtsFrame").HasColumnType("image").IsOptional();

            
            Property(t => t.CategoryId).HasColumnName("CategoryId").IsRequired();



        }
    }
}
