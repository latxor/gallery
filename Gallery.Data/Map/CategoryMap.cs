﻿using Gallery.Data.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Data.Map
{
    public class CategoryMap : EntityTypeConfiguration<Category>
    {
        public CategoryMap()
        {
            ToTable("Category");

            HasKey(t => t.Id);
            Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.Name).HasColumnName("Name").HasMaxLength(150).IsRequired();
            Property(t => t.Enable).HasColumnName("Enable").IsRequired();
            Property(t => t.Selected).HasColumnName("Selected").IsRequired();
            Property(t => t.CleanName).HasColumnName("CleanName").IsRequired();
            Property(t => t.UserId).HasColumnName("UserId").IsRequired();

            Property(t => t.GfyCatPanel).HasColumnName("GfyCatPanel").IsOptional();        
            Property(t => t.GifvPanel).HasColumnName("GifvPanel").IsOptional();
            Property(t => t.GifPanel).HasColumnName("GifPanel").IsOptional();
            Property(t => t.OtherImagePanel).HasColumnName("OtherImagePanel").IsOptional();









            //this.HasMany<Plan>(c => c.ListOfPlans)
            //    .WithRequired()
            //    .HasForeignKey(c => c.CategoryId);

            //this.HasMany<CityCategory>(c => c.ListOfCityCategory)
            //       .WithRequired()
            //       .HasForeignKey(c => c.CategoryId);
        }
    }
}
