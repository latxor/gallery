﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Data.Model
{
    public class gifvitem
    {
        public gifvitem()
        {

        }
        public gifvitem(string title, string link, string name, string gifv, string mp4, string webm)
        {
            this.title = title;
            this.link = link;
            this.name = name;
            this.gifv = gifv;
            this.mp4 = mp4;
            this.webm = webm;
        }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public virtual Gifv Gifv { get; set; }

        
        public string title { get; set; }
        public string description { get; set; }
        public int datetime { get; set; }
        public string type { get; set; }
        public bool animated { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int size { get; set; }
        public int views { get; set; }
        public string link { get; set; }
        public string name { get; set; }
        public string gifv { get; set; }
        public string mp4 { get; set; }
        public string webm { get; set; }       

    }
}
