﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gallery.Model;

namespace Gallery.Data.Model
{
    public class Gifv : Images
    {

        
        public virtual int CategoryId { get; set; }
        public virtual gifvitem gifvitem { get; set; }
        public ImageType Type { get { return ImageType.Gifv; } }


    }
}
