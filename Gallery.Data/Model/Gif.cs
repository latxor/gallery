﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Data.Model
{
    public class Gif: Images
    {
        public Byte[] FirstFrame { get; set; }
        
        public virtual int CategoryId { get; set; }
    }
}
