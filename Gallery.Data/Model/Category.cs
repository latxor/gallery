﻿using Gallery.Model.Interface;
using Rt.Core.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Gallery.Data.Model
{
    public class Category: ICategory,rtIModel
    {
        public Category()
        {
            GfyCat = new HashSet<GfyCat>();
            Gifv = new HashSet<Gifv>();
            Gif = new HashSet<Gif>();
            OthersImages = new HashSet<OthersImages>();
        }

        #region Propiedades Categoria
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Enable { get; set; }
        public bool Selected { get; set; }
        public string CleanName { get; set; }
        public int UserId { get; set; }

        public bool GfyCatPanel { get; set; }
        public bool GifvPanel { get; set; }
        public bool GifPanel { get; set; }
        public bool OtherImagePanel { get; set; }


        #endregion



        #region Propiedades Virtuales




        public virtual ICollection<GfyCat> GfyCat { get; set; }
        public virtual ICollection<Gifv> Gifv { get; set; }
        public virtual ICollection<Gif> Gif { get; set; }
        public virtual ICollection<OthersImages> OthersImages { get; set; }




        #endregion
    }
}
