﻿using Gallery.Model;
using Gallery.Model.Interface;
using Rt.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Data.Model
{
    public class Images : IImage, rtIModel
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public bool Enable { get; set; }
        public ImageType ImageType { get; set; }
        public string CleanUrl { get; set; }
        public ICollection<string> Tags { get; set; }



        
    }
}
