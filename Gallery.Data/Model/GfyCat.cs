﻿using Gallery.Model;
using Rt.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Data.Model
{
    public class GfyCat : Images
    {
        public gfyItem gfyItem { get; set; }
        public ImageType Type { get { return ImageType.GfyCat; } }

        public virtual int CategoryId { get; set; }
    }
}
