namespace Gallery.Data.Migrations
{
    using Model;
    using Rt.Core.Bussines.Factory;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Gallery.Data.Context.GalleryContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            
            //ContextKey = "Gallery.Data.Context.GalleryContext";
        }

        protected override void Seed(Gallery.Data.Context.GalleryContext context)
        {

            //context.Category.Add(new Category() { Name = "Default", Selected = true, Enable = true, CleanName = "Default" });
            //context.SaveChanges();




            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
