﻿#region Using Namespaces...

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data.Entity.Validation;

using Rt.Core.Data;
using Rt.Core.Data.Model;
using Rt.Core.Data.UnitOfWork;
using Gallery.Data.Context;
using Gallery.Data.Model;


#endregion

namespace Gallery.Data.UnitOfWork
{
    /// <summary>
    /// Unit of Work class responsible for DB transactions
    /// </summary>
    /// IDisposable, rgIUnitOfWork
    public class rtUnitOfWork : Rt.Core.Data.rtUnitOfWork 
    {
        #region Private member variables...

        
        private rtGenericRepository<Category> _categoryRepository;
        private rtGenericRepository<GfyCat> _gfycatRepository;
        private rtGenericRepository<Gifv> _gifvRepository;
        private rtGenericRepository<Gif> _gifRepository;
        private rtGenericRepository<OthersImages> _otherImagesRepository;




        #endregion


        public rtUnitOfWork()
            : base(new GalleryContext(string.Format(@"Server={0};  Database={1};User Id={2};  Password={3};",
                                @".\DESARROLLO",
                                "dbGallery",
                                "sa",
                                "sapassword")))
        {
        }


        public void CreateDB()
        {
            _context.Database.CreateIfNotExists();
        }
        //#region Public Repository Creation properties...



        public rtGenericRepository<Category> CategoryRepository
        {
            get
            {
                if (this._categoryRepository == null)
                    this._categoryRepository = new rtGenericRepository<Category>(_context);
                return _categoryRepository;
            }
        }

        public rtGenericRepository<GfyCat> GfyCatRepository
        {
            get
            {
                if (this._gfycatRepository == null)
                    this._gfycatRepository = new rtGenericRepository<GfyCat>(_context);
                return _gfycatRepository;
            }
        }



        public rtGenericRepository<Gifv> GifvRepository
        {
            get
            {
                if (this._gifvRepository == null)
                    this._gifvRepository = new rtGenericRepository<Gifv>(_context);
                return _gifvRepository;
            }
        }


        public rtGenericRepository<Gif> GifRepository
        {
            get
            {
                if (this._gifRepository == null)
                    this._gifRepository = new rtGenericRepository<Gif>(_context);
                return _gifRepository;
            }
        }
        public rtGenericRepository<OthersImages> OthersImagesRepository
        {
            get
            {
                if (this._otherImagesRepository == null)
                    this._otherImagesRepository = new rtGenericRepository<OthersImages>(_context);
                return _otherImagesRepository;
            }
        }



    }
}