﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gallery.Signal
{
    [HubName("galleryhub")]
    public class GalleryHub: Hub
    {
        private IHubContext hubContext;
        public GalleryHub()
        {
            hubContext = GlobalHost.ConnectionManager.GetHubContext<GalleryHub>();

        }
        public void Send(bool success, string name)
        {
            hubContext.Clients.All.Message(success,name);
        }
    }
}