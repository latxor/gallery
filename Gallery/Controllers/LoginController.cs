﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;

using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Rt.Core.Bussines.Services;
using Gallery.Models;
using Rt.Core.Data.Model;

namespace Gallery.Controllers
{
    
    public class LoginController : Controller
    {
        private rtSignInServices _signInManager;
        private rtUserServices _userManager;
        
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public LoginController()
        {
        }

        public LoginController(rtUserServices userManager, rtSignInServices signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            

        }

        public rtSignInServices SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<rtSignInServices>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public rtUserServices UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<rtUserServices>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            rtUser user = new rtUser() { UserName = "krlos.g@gmail.com", EmailConfirmed = true, Email = "krlos.g@gmail.com" };
            UserManager.Create(user, "Abc_123");
            LoginViewModel account = new LoginViewModel();       
            return View(account);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToAction("Index", "Home");
                case SignInStatus.LockedOut:
                    ModelState.AddModelError("", "Usuario bloqueado temporalmente.");
                    return View(model);
                default:
                    ModelState.AddModelError("", "Usuario o contraseña invalida.");
                    return View(model);
            }

            
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new rtUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("Index", "Task");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View("Index",model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);            
            return RedirectToAction("Index", "Login");
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}