﻿using Gallery.Data.Model;
using Gallery.Repository;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Gallery.Bussines.Factory;
using Gallery.Data.Context;
using Gallery.Signal;
using System.Threading.Tasks;
using Rt.Core.Data.Model;
using Microsoft.AspNet.Identity;
using Rt.Core.Bussines.Factory;

namespace Gallery.Controllers
{
    [Authorize]
    //[RequireHttps]
    public class HomeController : Controller
    {
        GalleryHub hub;
        
        public HomeController()
        {
            hub = new GalleryHub();
        }

        public ActionResult Index()
        {

            var id = User.Identity.GetUserId<int>();
            Category Category = rtServicesFactory.CategoryServices().GetCategoryBySelected(true, User.Identity.GetUserId<int>());
            
            if (Category==null)           
                Category = new Category();
            
            return View(Category);
        }

        public ActionResult FindImages(int id)
        {
            Category NewCategorySelected = new Category();
            int userid = User.Identity.GetUserId<int>();
            var OldsCategorySelected = rtServicesFactory.CategoryServices().GetItemBySelected(true,userid);

            foreach (var item in OldsCategorySelected)
            {
                item.Selected = false;
                rtServicesFactory.CategoryServices().Update(item,userid);
            }
            
            NewCategorySelected = rtServicesFactory.CategoryServices().GetItemById(id,userid);        
            NewCategorySelected.Selected = true;
            rtServicesFactory.CategoryServices().Update(NewCategorySelected, userid);
                                  
            return PartialView("_Images", NewCategorySelected);
            
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult CreateCategory(string name)
        {
            int userid = User.Identity.GetUserId<int>();
            if (!string.IsNullOrWhiteSpace(name))
            {
                var listcategoriesselected = rtServicesFactory.CategoryServices().GetAll(userid).Where(c=>c.Selected== true);
                Category category = new Category() { Name = name, Enable = true, UserId = userid, CleanName= UrlFactory.RemoveSpecialCharacters(name) };
                //Sino tiene categoria seleccionada
                if (listcategoriesselected.Count()<=0)
                {
                    category.Selected = true;
                }
               
                var result = rtServicesFactory.CategoryServices().Create(category, userid);
                if (result > 0)
                    hub.Send(true, "Categoria Creada");
                else
                    hub.Send(false, "Error al crear la nueva categoria");

            }
            else
                hub.Send(false, "Nombre Invalido");


            var categories = rtServicesFactory.CategoryServices().GetAll(userid);
            return PartialView("_Categories", categories);
            
           
        }

        public ActionResult GetAllCategoriesUser()
        {
            int userid = User.Identity.GetUserId<int>();
            List<Category> ListOfCategories = rtServicesFactory.CategoryServices().GetAll(userid).ToList();
            return PartialView("_Categories", ListOfCategories);
        }

        //public ActionResult CollapseCategory(int id, bool collapse)
        //{
        //    return Json(new { collapse }, JsonRequestBehavior.AllowGet);
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult FindCategories(string term)
        {
            int userid = User.Identity.GetUserId<int>();
            List<Category> categories = new List<Category>();
            if (!string.IsNullOrWhiteSpace(term))
                categories = rtServicesFactory.CategoryServices().GetItemByName(term,userid);
            else
                categories = rtServicesFactory.CategoryServices().GetAll(userid).ToList();
            return PartialView("_Categories", categories);
           
          
        }

        public ActionResult DeleteCategory(int id)
        {
            bool result = false;
            int categoryselectedid = 0;
            int userid = User.Identity.GetUserId<int>();
            result = rtServicesFactory.CategoryServices().Delete(id, userid);
            if (result)
            {
                var category = rtServicesFactory.CategoryServices().GetAll(userid).FirstOrDefault();
                category.Selected = true;
                result = rtServicesFactory.CategoryServices().Update(category, userid);
                if (result)
                    categoryselectedid = category.Id;

            }

            return Json(new { sucess = result, categoryselectedid = categoryselectedid }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult UpdateCategoryList()
        {
            int userid = User.Identity.GetUserId<int>();
            List<SelectListItem> ListOfCategories = new List<SelectListItem>();
            var categories = rtServicesFactory.CategoryServices().GetAll(userid);

            foreach (var category in categories)
            {
                ListOfCategories.Add(new SelectListItem() { Text = category.Name, Value = category.Id.ToString() });
            }
            ViewBag.ListOfCategories = ListOfCategories;
            return PartialView("_CreateItem");
        }

        [HttpPost]
        public ActionResult SavePanelConfiguration(string type, bool open)
        {
            int userid = User.Identity.GetUserId<int>();
            Category categoryselected = rtServicesFactory.CategoryServices().GetItemBySelected(true, userid).FirstOrDefault();
            var result = false;
            if (categoryselected!=null)
            {
                switch (type)
                {
                    case "GfyCat":
                        categoryselected.GfyCatPanel = open;
                        break;
                    case "Gifv":
                        categoryselected.GifvPanel = open;
                        break;
                    case "Gif":
                        categoryselected.GifPanel = open;
                        break;
                    case "OtherImage":
                        categoryselected.OtherImagePanel = open;                        
                        break;
                }

                result = rtServicesFactory.CategoryServices().Update(categoryselected, 1);
            }
            return Json(new { data= result},JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult CreateItem(int categorySelectd, int typeOption, string url)
        {
            int result=-1;
            switch (typeOption)
            {
                case 1:
                    GfyCat NewGfyCat = new GfyCat() { CategoryId = categorySelectd, Url = url, CleanUrl= url};
                    result = rtServicesFactory.GfyCatServices().Create(NewGfyCat, 1);
                    break;
                case 2:
                    Gifv NewGifv = new Gifv() { CategoryId = categorySelectd, Url = url, CleanUrl = url };
                    result = rtServicesFactory.GifvServices().Create(NewGifv, 1);
                    break;
                case 3:
                    Gif NewGif = new Gif() { CategoryId = categorySelectd, Url = url, CleanUrl = UrlFactory.RemoveSpecialCharacters(url) };
                    result = rtServicesFactory.GifServices().Create(NewGif, 1);
                    break;
                case 4:
                    OthersImages NewOthersImages = new OthersImages() { CategoryId = categorySelectd, Url = url, CleanUrl = UrlFactory.RemoveSpecialCharacters(url) };
                    result = rtServicesFactory.OtherImagesServices().Create(NewOthersImages, 1);
                    break;

                default:
                    break;
            }

            if (result > 0)
                hub.Send(true, "Imagen Creada");
            else
                hub.Send(false, "Error al crear la imagen");

            return RedirectToAction("Index");
        }

        public ActionResult DeleteImage(int id,int type)
        {

            Images image = new Images();
            bool result = false;
            switch (type)
            {
                case 1:
                    var gfy = rtServicesFactory.GfyCatServices().GetItemById(id);
                    result = rtServicesFactory.GfyCatServices().Delete(id,User.Identity.GetUserId<int>());
                    image = gfy as Images;
                    break;
                case 2:
                    var gifv = rtServicesFactory.GifvServices().GetItemById(id);
                    result = rtServicesFactory.GifvServices().Delete(id, User.Identity.GetUserId<int>());
                    image = gifv as Images;
                    break;
                case 3:
                    var gif = rtServicesFactory.GifServices().GetItemById(id);
                    result = rtServicesFactory.GifServices().Delete(id, User.Identity.GetUserId<int>());
                    image = gif as Images;
                    break;
                case 4:
                    var otherimage = rtServicesFactory.OtherImagesServices().GetItemById(id);
                    result = rtServicesFactory.OtherImagesServices().Delete(id, User.Identity.GetUserId<int>());
                    image = otherimage as Images;
                    break;
                default:
                    break;
            }
            //string prua = id.ToString();
            //var image = rtServicesFactory.GfyCatServices().GetItemById(id);
            return Json(new { id = image.Id.ToString()+ "_" + image.CleanUrl, sucess = result }, JsonRequestBehavior.AllowGet);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            int userid = User.Identity.GetUserId<int>();
            List<SelectListItem> ListOfCategories = new List<SelectListItem>();
            ViewBag.Categories = rtServicesFactory.CategoryServices().GetAll(userid).ToList(); ;
            foreach (var category in ViewBag.Categories)
            {
                ListOfCategories.Add(new SelectListItem() { Text = category.Name, Value = category.Id.ToString() });
            }

            ViewBag.ListOfCategories = ListOfCategories;
            
            base.OnActionExecuting(filterContext);

        }

        
    }


  
}