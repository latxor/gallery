﻿using Gallery.Models;
using Gallery.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gallery.Repository
{
    public static class Repository
    {
        private static List<Category> Categories;
        public static List<Category> GetData()
        {
            Categories = new List<Category>();
            Categories.Add(new Category()
            {
                Id = 1,
                Name = "K",
                GfyCatList = new List<GfyCat>()
                 {
                       new GfyCat()
                       {
                            
                            Url = "SlushyUltimateCranefly",
                            CategoryId=1,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("SlushyUltimateCranefly").Content).gfyItem
                       },

                        new GfyCat()
                       {
                            
                            Url = "ShabbyGaseousGerbil",
                            CategoryId=1,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("ShabbyGaseousGerbil").Content).gfyItem
                       },

                         new GfyCat()
                       {
                            
                            Url = "AromaticMetallicHound",
                            CategoryId=1,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("AromaticMetallicHound").Content).gfyItem
                       },

                          new GfyCat()
                       {
                            
                            Url = "IcyEarlyBeagle",
                            CategoryId=1,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("IcyEarlyBeagle").Content).gfyItem
                       },

                           new GfyCat()
                       {
                            
                            Url = "ColdRewardingArctichare",
                            CategoryId=1,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("ColdRewardingArctichare").Content).gfyItem
                       },




                        new GfyCat()
                       {
                            
                            Url = "SickJointAmbushbug",
                            CategoryId=1,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("SickJointAmbushbug").Content).gfyItem
                       },

                       new GfyCat()
                       {
                            
                            Url = "WateryPerkyBlackbear",
                            CategoryId=1,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("WateryPerkyBlackbear").Content).gfyItem
                       },

                       new GfyCat()
                       {
                            
                            Url = "ExcellentFragrantHammerheadshark",
                            CategoryId=1,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("ExcellentFragrantHammerheadshark").Content).gfyItem
                       },

                        new GfyCat()
                       {
                            
                            Url = "LinedMaleAmericanrobin",
                            CategoryId=1,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("LinedMaleAmericanrobin").Content).gfyItem
                       },

                       new GfyCat()
                       {
                            
                            Url = "FaintInsistentAsianporcupine",
                            CategoryId=1,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("FaintInsistentAsianporcupine").Content).gfyItem
                       },


                       new GfyCat()
                       {
                            
                            Url = "WelltodoBitesizedAzurevasesponge",
                            CategoryId=1,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("WelltodoBitesizedAzurevasesponge").Content).gfyItem
                       },

                       new GfyCat()
                       {
                            
                            Url = "AnyOfficialDromaeosaur",
                            CategoryId=1,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("AnyOfficialDromaeosaur").Content).gfyItem
                       },

                       new GfyCat()
                       {
                            
                            Url = "RepulsiveVigorousJenny",
                            CategoryId=1,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("RepulsiveVigorousJenny").Content).gfyItem
                       },

                       new GfyCat()
                       {
                            
                            Url = "GiganticFirsthandBorzoi",
                            CategoryId=1,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("GiganticFirsthandBorzoi").Content).gfyItem
                       },



                  }
            });


              Categories.Add(new Category()
            {
                Id = 2,
                Name = "Lost",
                GfyCatList = new List<GfyCat>()
                 {
                       new GfyCat()
                       {
                            
                            Url = "AmusingHiddenBonobo",
                            CategoryId=2,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("AmusingHiddenBonobo").Content).gfyItem
                       },

                        new GfyCat()
                       {
                            
                            Url = "WellinformedOldfashionedAmericancreamdraft",
                            CategoryId=2,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("WellinformedOldfashionedAmericancreamdraft").Content).gfyItem
                       },

                         new GfyCat()
                       {
                            
                            Url = "KindChillyHammerheadbird",
                            CategoryId=2,
                            gfyItem =  JsonConvert.DeserializeObject<GfyCat>(ImageRepository.GetGfyCat("KindChillyHammerheadbird").Content).gfyItem
                       },

                      


                  }
            });

          
            return Categories;

        }



    }
}