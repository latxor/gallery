﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.WebPages;

namespace Gallery.Repository
{
    public static class AjaxHtmlHelpers
    {
        public static IHtmlString ActionLink<T>(this AjaxHelper ajaxHelper, T item, Func<T, HelperResult> template, string action, string controller, object routeValues, AjaxOptions options)
        {
            string rawContent = template(item).ToHtmlString();
            MvcHtmlString a = ajaxHelper.ActionLink("$$$", action,
                controller, routeValues, options);
            return MvcHtmlString.Create(a.ToString().Replace("$$$", rawContent));
        }

        public static MvcHtmlString RawActionLink(this AjaxHelper ajaxHelper, string linkText, string actionName, string controllerName, object routeValues, AjaxOptions ajaxOptions, object htmlAttributes)
        {
            var repID = Guid.NewGuid().ToString();
            var lnk = ajaxHelper.ActionLink(repID, actionName, controllerName, routeValues, ajaxOptions, htmlAttributes);
            return MvcHtmlString.Create(lnk.ToString().Replace(repID, linkText));
        }
    }
}