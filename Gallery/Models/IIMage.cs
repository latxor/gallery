﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Models
{
    public interface IImage
    {
        int Id { get; set; }
        string Url { get; set; }
        int CategoryId { get; set; }
        
    }
}
