﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gallery.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Enable { get; set; }
        public bool Selected { get; set; }
        
        public ICollection<GfyCat> GfyCatList { get; set; }
    }
}