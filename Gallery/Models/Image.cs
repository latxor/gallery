﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gallery.Models
{
    public class Image: IImage
    {
        public int Id { get; set; }
        public string Url { get; set; }        
        public int CategoryId { get; set; }

    }

    public enum Type:int
    {
        GfyCat,
        Gifv,
        Gif
    }
}