﻿using Gallery.Controllers;
using Gallery.Data.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Gallery.Test.Controller
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index_Should_Return_View_With_Category()
        {

           
            var controller = new HomeController();
            controller.ControllerContext = GetContext();
            
            var result = controller.Index() as ViewResult;
            var category = (Category)result.ViewData.Model;

            Assert.IsTrue("" == result.ViewName);
            Assert.IsTrue(category != null);

           
        }

        [TestMethod]
        public void FindImages_Should_Return_PartialView_With_Category()
        {
            var controller = new HomeController();
            controller.ControllerContext = GetContext();

            var result = controller.FindImages(1) as PartialViewResult;
            var category = (Category)result.ViewData.Model;

            Assert.IsTrue("_Images" == result.ViewName);
            Assert.IsTrue(category.Selected);
        }

        [TestMethod]
        public void CreateCategory_Should_Return_PartialView_With_List_Of_Categories()
        {
            var controller = new HomeController();
            controller.ControllerContext = GetContext();

            var result = controller.CreateCategory("New Category") as PartialViewResult;
            var listofcategories =(List<Category>) result.ViewData.Model;

            Assert.IsTrue("_Categories" == result.ViewName);
            Assert.IsTrue(listofcategories != null);
            Assert.IsTrue(listofcategories.Count >= 0);
        }

        [TestMethod]
        public void GetAllCategoriesUser_Should_Return_PartialView_With_List_Of_Categories()
        {
            var controller = new HomeController();
            controller.ControllerContext = GetContext();

            var result = controller.GetAllCategoriesUser() as PartialViewResult;
            var listofcategories = (List<Category>) result.ViewData.Model;

            Assert.AreEqual("_Categories", result.ViewName);
            Assert.IsTrue(listofcategories.Count >= 0);
            Assert.IsNotNull(listofcategories);
        }

        [TestMethod]
        public void FindCategorie_Should_Return_List_Of_Categories()
        {
            var controller = new HomeController();
            controller.ControllerContext = GetContext();

            var result = controller.FindCategories("n")  as PartialViewResult;
            var listofcategories = (List<Category>)result.ViewData.Model;

            Assert.AreEqual("_Categories", result.ViewName);
            Assert.IsTrue(listofcategories.Count >= 0);
            Assert.IsNotNull(listofcategories);
        }

        [TestMethod]
        public void FindCategorie_Should_Return_Empty_List_Of_Categories()
        {
            var controller = new HomeController();
            controller.ControllerContext = GetContext();

            var result = controller.FindCategories("xxxxx") as PartialViewResult;
            var listofcategories = (List<Category>)result.ViewData.Model;

            Assert.AreEqual("_Categories", result.ViewName);
            Assert.IsTrue(listofcategories.Count == 0);
            Assert.IsNotNull(listofcategories);
        }

        [TestMethod]
        public void UpdateCategory_Should_Return_List_Of_SelectListItem()
        {
            var controller = new HomeController();
            controller.ControllerContext = GetContext();

            var result = controller.UpdateCategoryList() as PartialViewResult;
            var selectListItem = (List<SelectListItem>)result.ViewData["ListOfCategories"];

            Assert.IsNotNull(selectListItem);
            Assert.IsTrue(selectListItem.Count >= 0);
            Assert.AreEqual("_CreateItem", result.ViewName);
        }

        [TestMethod]
        public void SavePanelConfiguration_Should_Return_Json()
        {
            var controller = new HomeController();
            controller.ControllerContext = GetContext();

            var result = controller.SavePanelConfiguration("Gif", true) as JsonResult;

            Assert.IsNotNull(result);
            
        }

        [TestMethod]
        public void CreateItem_Should_Return_RedirectToAction()
        {
            var controller = new HomeController();
            controller.ControllerContext = GetContext();

            var result = (RedirectToRouteResult) controller.CreateItem(1, 1, "shabbygaseousgerbil") ;
            Assert.AreEqual("Index", result.RouteValues["action"]);
            
        }

        private class TestableControllerContext : ControllerContext
        {
            public TestableHttpContext TestableHttpContext { get; set; }
        }

        private class TestableHttpContext : HttpContextBase
        {
            public override IPrincipal User { get; set; }
        }

        private TestableControllerContext GetContext()
        {
            var identity = new GenericIdentity("leo@leo.com");
            identity.AddClaim(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", "1", "http://www.w3.org/2001/XMLSchema#string", "LOCAL AUTHORITY", "LOCAL AUTHORITY"));

            identity.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "ASP.NET Identity", "http://www.w3.org/2001/XMLSchema#string", "LOCAL AUTHORITY", "LOCAL AUTHORITY"));

            identity.AddClaim(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", "leo@leo.com", "http://www.w3.org/2001/XMLSchema#string", "LOCAL AUTHORITY", "LOCAL AUTHORITY"));
            var controllerContext = new TestableControllerContext();
            var principal = new GenericPrincipal(identity, null);
            var testableHttpContext = new TestableHttpContext
            {
                User = principal
            };

            controllerContext.HttpContext = testableHttpContext;
            return controllerContext;
        }
    }
}
