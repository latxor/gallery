﻿using Gallery.Bussines.Factory;
using Gallery.Data.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rt.Core.Bussines.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Test.Bussiness
{
    [TestClass]
    public class CategoryServicesTest
    {
        [TestMethod]
        public void Create()
        {
            var result = rtServicesFactory.CategoryServices().Create(New(), 1);
            Assert.IsTrue(result > 0, "Sucess");
        }

        [TestMethod]
        public void GetById()
        {
            var result = rtServicesFactory.CategoryServices().Create(New(), 1);
            var categoy = rtServicesFactory.CategoryServices().GetItemById(result,1);
            Assert.IsTrue(categoy !=null);
            Assert.IsTrue(categoy.Id==result);

        }

        [TestMethod]
        public void GetAll()
        {
            var result = rtServicesFactory.CategoryServices().GetAll(1);
            Assert.IsTrue(result.Count() > 0);
        }

        [TestMethod]
        public void GetByName()
        {
            var result = rtServicesFactory.CategoryServices().GetItemByName("New",1);

            Assert.IsTrue(result.Count() > 0);
        }

        [TestMethod]
        public void Delete()
        {
            var id = rtServicesFactory.CategoryServices().Create(New(), 1);
            var result = rtServicesFactory.CategoryServices().Delete(id, 1);
            Assert.IsTrue(id >0);
            Assert.IsTrue(result ==true);
        }
        private Category New()
        {
            Category category = new Category()
            {
                UserId = 1,
                Name = "New Category",
                CleanName= UrlFactory.RemoveSpecialCharacters("New Category")
            };

            return category;
        }

}
}
