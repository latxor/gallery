﻿using Gallery.Bussines.Factory;
using Gallery.Data.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Test.Bussiness
{
    [TestClass]
    public class GifvServicesTest
    {
        [TestMethod]
        public void Create()
        {
            Gifv image = New();
            var result = rtServicesFactory.GifvServices().Create(image, 1);
            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public void Delete()
        {
            Gifv image = New();
            var id = rtServicesFactory.GifvServices().Create(image, 1);
            var result = rtServicesFactory.GifvServices().Delete(id, 1);
            Assert.IsTrue(id > 0);
            Assert.IsTrue(result == true);

        }

        private Gifv New()
        {
            Gifv image = new Gifv()
            {
                CategoryId = 1,
                Url = "76tVwoI",
                CleanUrl = "76tVwoI"
            };

            return image;

        }
    }
}
