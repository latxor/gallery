﻿using Gallery.Bussines.Factory;
using Gallery.Data.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rt.Core.Bussines.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Test.Bussiness
{
    [TestClass]
    public class GifServicesTest
    {

        
        [TestMethod]
        public void Create()
        {
            Gif image = New();
            var result = rtServicesFactory.GifServices().Create(image, 1);
            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public void Delete()
        {
            Gif image = New();
            var id = rtServicesFactory.GifServices().Create(image, 1);
            var result = rtServicesFactory.GifServices().Delete(id, 1);
            Assert.IsTrue(id > 0);
            Assert.IsTrue(result == true);

        }

        private Gif New()
        {
            Gif image = new Gif()
            {
                CategoryId = 1,
                Url = "http://i.imgur.com/pibPJ.gif",
                CleanUrl = UrlFactory.RemoveSpecialCharacters("http://i.imgur.com/pibPJ.gif")
            };

            return image;

        }
    }
}
