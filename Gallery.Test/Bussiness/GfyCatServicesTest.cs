﻿using Gallery.Bussines.Factory;
using Gallery.Data.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Test.Bussiness
{
    [TestClass]
    public class GfyCatServicesTest
    {
        [TestMethod]
        public void Create()
        {
            GfyCat image = New();
            var result = rtServicesFactory.GfyCatServices().Create(image, 1);
            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public void Delete()
        {
            GfyCat image = New();
            var id = rtServicesFactory.GfyCatServices().Create(image, 1);
            var result = rtServicesFactory.GfyCatServices().Delete(id, 1);
            Assert.IsTrue(id > 0);
            Assert.IsTrue(result==true);

        }

        private GfyCat New()
        {
            GfyCat image = new GfyCat()
            {
                CategoryId = 1,
                Url = "marvelousimaginativearawana",
                CleanUrl = "marvelousimaginativearawana"
            };

            return image;

        }
    }
}
