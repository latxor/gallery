﻿using Gallery.Bussines.Factory;
using Gallery.Data.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rt.Core.Bussines.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Test.Bussiness
{
    [TestClass]
    public class OtherImagesServicesTest
    {


        [TestMethod]
        public void Create()
        {
            OthersImages image = New();
            var result = rtServicesFactory.OtherImagesServices().Create(image, 1);
            Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public void Delete()
        {
            OthersImages image = New();
            var id = rtServicesFactory.OtherImagesServices().Create(image, 1);
            var result = rtServicesFactory.OtherImagesServices().Delete(id, 1);
            Assert.IsTrue(id > 0);
            Assert.IsTrue(result == true);

        }

        private OthersImages New()
        {
            OthersImages image = new OthersImages()
            {
                CategoryId = 1,
                Url = "http://i.imgur.com/0aQAEzr.jpg",
                CleanUrl = UrlFactory.RemoveSpecialCharacters("http://i.imgur.com/0aQAEzr.jpg")
            };

            return image;

        }
    }
}
