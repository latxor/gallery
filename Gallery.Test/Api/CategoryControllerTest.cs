﻿using Gallery.API.Controllers;
using Gallery.Data.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace Gallery.Test.Api
{
    [TestClass]
    public class CategoryControllerTest
    {
        CategoryController controller;
        [TestInitialize]
        public void Initialize()
        {
            controller = new CategoryController();

            var identity = new GenericIdentity("carlos@carlos.com");
            identity.AddClaim(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", "2", "http://www.w3.org/2001/XMLSchema#string", "LOCAL AUTHORITY", "LOCAL AUTHORITY"));

            identity.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "ASP.NET Identity", "http://www.w3.org/2001/XMLSchema#string", "LOCAL AUTHORITY", "LOCAL AUTHORITY"));

            identity.AddClaim(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", "carlos@carlos.com", "http://www.w3.org/2001/XMLSchema#string", "LOCAL AUTHORITY", "LOCAL AUTHORITY"));
            controller.User = new GenericPrincipal(identity, null);

        }

        [TestMethod, TestCategory("Api")]
        public void Get()
        {
                
            var actionResult = controller.Get();
            var response = actionResult as OkNegotiatedContentResult<IEnumerable<Category>>;
            Assert.IsNotNull(response);
            var categories = response.Content;
            Assert.IsTrue(categories.Count()>0);
        }
        [TestMethod, TestCategory("Api")]
        public void Create()
        {
            var actionResult = controller.Create("Nueva Categoria");
            var response = actionResult as OkNegotiatedContentResult<bool>;
            Assert.IsTrue(response.Content);
        }

        [TestMethod, TestCategory("Api")]
        public void Create_Should_Return_Bad_Request()
        {
            var actionResult = controller.Create("");
            var response = actionResult as BadRequestResult;
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod, TestCategory("Api")]
        public void FindCategoryByName()
        {
            var actionResult = controller.FindCategory("N");
            var response = actionResult as OkNegotiatedContentResult<IEnumerable<Category>>;
            Assert.IsNotNull(response);
            var categories = response.Content;
            Assert.IsTrue(categories.Count() > 0);
        }


        [TestMethod, TestCategory("Api")]
        public void FindCategoryById()
        {
            var actionResult = controller.FindCategoryById(3);
            var response = actionResult as OkNegotiatedContentResult<Category>;
            Assert.IsNotNull(response);
            var categories = response.Content;
            Assert.IsTrue(categories.Id == 3);
        }

        [TestMethod, TestCategory("Api")]
        public void AddImageToCategory()
        {
            var actionResult = controller.AddImageToCategory(3, 1, "insistentcarefreeharborseal");
            var response = actionResult as OkNegotiatedContentResult<bool>;
            Assert.IsTrue(response.Content);
        }


    }
}
