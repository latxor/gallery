﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rt.Core.Bussines.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.Test.Helper
{
    [TestClass]
    public class UrlHelper
    {
        [TestMethod]
        public void RemoveSpecialsCharacters()
        {
            string name = "New;Category_:;";
            Assert.AreEqual("NewCategory", UrlFactory.RemoveSpecialCharacters(name));
        }
    }
}
