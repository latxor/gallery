﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.UWP.Data.Enum
{
    public enum ImageType
    {
        GfyCat,
        Gifv,
        Gif,
        Jpg_Png
    }
}
