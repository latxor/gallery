﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.UWP.Data.Interface
{
    public interface ICategory
    {
        int Id { get; set; }
        string Name { get; set; }
        bool Enable { get; set; }
    }
}
