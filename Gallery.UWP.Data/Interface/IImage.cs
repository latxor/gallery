﻿using Gallery.UWP.Data.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.UWP.Data.Interface
{
    public interface IImage
    {
        int Id { get; set; }
        string Url { get; set; }
        bool Enable { get; set; }
        ImageType ImageType { get; set; }
        ICollection<string> Tags { get; set; }
    }
}
