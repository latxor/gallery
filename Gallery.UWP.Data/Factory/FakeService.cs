﻿using Gallery.UWP.Data;
using Gallery.UWP.Data.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.UWP.Data.Factory
{
    public class FakeService
    {
        public static String Name = "Fake Data Service.";

        public static List<Person> GetPeople()
        {
            Debug.WriteLine("GET for people.");
            return new List<Person>()
                {
                    new Person() { Name="Chris Cole", Age=10 },
                    new Person() { Name="Kelly Kale", Age=32 },
                    new Person() { Name="Dylan Durbin", Age=18 }
                };
        }

        public static List<Category> GetCategories()
        {
            Debug.WriteLine("GET for categories.");
            return new List<Category>()
                {
                    new Category() { Name="K", Id=1 },
                    new Category() { Name="TL;TR", Id=2 },
                    new Category() { Name="Fail", Id=2 }
                };
        }


        public static void Write(Person person)
        {
            Debug.WriteLine("INSERT person with name " + person.Name);
        }

        public static void Delete(Person person)
        {
            Debug.WriteLine("DELETE person with name " + person.Name);
        }

        public static void Write(Category Category)
        {
            Debug.WriteLine("INSERT Category with name " + Category.Name);
        }

        public static void Delete(Category Category)
        {
            Debug.WriteLine("DELETE Category with name " + Category.Name);
        }
    }
}
