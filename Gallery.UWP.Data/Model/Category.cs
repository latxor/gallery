﻿using Gallery.UWP.Data.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.UWP.Data.Model
{
    public class Category:ICategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Enable { get; set; }
        public bool Selected { get; set; }
        public string CleanName { get; set; }
        public int UserId { get; set; }

        public bool GfyCatPanel { get; set; }
        public bool GifvPanel { get; set; }
        public bool GifPanel { get; set; }
        public bool OtherImagePanel { get; set; }

        public virtual ICollection<GfyCat> GfyCat { get; set; }
        public virtual ICollection<Gifv> Gifv { get; set; }
        public virtual ICollection<Gif> Gif { get; set; }
        public virtual ICollection<OthersImages> OthersImages { get; set; }

        public void UpdateGfyCat(GfyCat person)
        {
            
        }
        public void UpdateGifv(Gifv person)
        {

        }

        public void UpdateGif(Gif person)
        {

        }

        public void UpdateOtherImages(OthersImages person)
        {

        }
    }
}
