﻿
using Gallery.UWP.Data.Enum;
using Gallery.UWP.Data.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.UWP.Data.Model
{
    public class Images : IImage
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public bool Enable { get; set; }
        public ImageType ImageType { get; set; }
        public string CleanUrl { get; set; }
        public ICollection<string> Tags { get; set; }



        
    }
}
