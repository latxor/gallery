﻿
using Gallery.UWP.Data.Enum;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.UWP.Data.Model
{
    public class GfyCat : Images
    {
        public gfyItem gfyItem { get; set; }
        public ImageType Type { get { return ImageType.GfyCat; } }

        public virtual int CategoryId { get; set; }
    }
}
