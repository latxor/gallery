﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.UWP.Data.Model
{
    public class Gif: Images
    {
        public Byte[] FirstFrame { get; set; }
        
        public virtual int CategoryId { get; set; }
    }
}
