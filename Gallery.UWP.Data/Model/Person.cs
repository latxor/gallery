﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.UWP.Data.Model
{
    public class Person
    {
        public String Name { get; set; }
        public int Age { get; set; }
    }
}
