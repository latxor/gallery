﻿using Gallery.UWP.Data.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.UWP.Data.Model
{
    public class OFAGallery
    {
        public List<Category> Categories { get; set; }        
        public OFAGallery()
        {
            Categories = new List<Category>();
            Categories = FakeService.GetCategories();
        }

        public void Add(Category Category)
        {
            if (!Categories.Contains(Category))
            {
                Categories.Add(Category);
                FakeService.Write(Category);
            }
        }

        public void Delete(Category Category)
        {
            if (Categories.Contains(Category))
            {
                Categories.Remove(Category);
                FakeService.Delete(Category);
            }
        }

        public void Update(Category Category)
        {
            FakeService.Write(Category);
        }
    }
}
