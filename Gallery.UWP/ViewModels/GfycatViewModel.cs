﻿using Gallery.UWP.Data.Enum;
using Gallery.UWP.Data.Helper;
using Gallery.UWP.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.UWP.ViewModels
{
    public class GfycatViewModel:  NotificationBase<GfyCat>
    {
        public GfycatViewModel(GfyCat gfycat = null) : base(gfycat)
        { }

        public gfyItem gfyItem
        {
            get { return This.gfyItem; }
            set { SetProperty(This.gfyItem, value, () => This.gfyItem = value); }
        }
        public ImageType Type { get { return ImageType.GfyCat; } }

    

        public int CategoryId
        {
            get { return This.CategoryId; }
            set { SetProperty(This.CategoryId, value, () => This.CategoryId = value); }
        }

        #region Commons Properties

        public int Id
        {
            get { return This.Id; }
            set { SetProperty(This.Id, value, () => This.Id = value); }
        }

        public string Url
        {
            get { return This.Url; }
            set { SetProperty(This.Url, value, () => This.Url = value); }
        }


        public bool Enable
        {
            get { return This.Enable; }
            set { SetProperty(This.Enable, value, () => This.Enable = value); }
        }

        public string CleanUrl
        {
            get { return This.CleanUrl; }
            set { SetProperty(This.CleanUrl, value, () => This.CleanUrl = value); }
        }       

        #endregion




    }
}
