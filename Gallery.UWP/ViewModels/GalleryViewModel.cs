﻿using Gallery.UWP.Data.Helper;
using Gallery.UWP.Data.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.UWP.ViewModels
{
    public class GalleryViewModel: NotificationBase
    {

        OFAGallery gallery;
        public GalleryViewModel()
        {
            gallery = new OFAGallery();
            _SelectedIndex = -1;
            foreach (var category in gallery.Categories)
            {
                var np = new CategoryViewModel(category);
                np.PropertyChanged += Category_OnNotifyPropertyChanged;
                _Category.Add(np);
            }

        }


        ObservableCollection<CategoryViewModel> _Category = new ObservableCollection<CategoryViewModel>();
        public ObservableCollection<CategoryViewModel> Category
        {
            get { return _Category; }
            set { SetProperty(ref _Category, value); }
        }


        int _SelectedIndex;
        public int SelectedIndex
        {
            get { return _SelectedIndex; }
            set
            {
                if (SetProperty(ref _SelectedIndex, value))
                { RaisePropertyChanged(nameof(SelectedCategory)); }
            }
        }
        public CategoryViewModel SelectedCategory
        {
            get { return (_SelectedIndex >= 0) ? _Category[_SelectedIndex] : null; }
        }
      

        public void Add()
        {
            var Id = Category.Count + 1;
            var category = new CategoryViewModel() {Name="prueba" + Id.ToString(), Id = Id };
            category.PropertyChanged += Category_OnNotifyPropertyChanged;
            Category.Add(category);
            gallery.Add(category);
            SelectedIndex = Category.IndexOf(category);
          
        }

        public void Delete()
        {
            if (SelectedIndex != -1)
            {
                var category = Category[SelectedIndex];
                Category.RemoveAt(SelectedIndex);
                gallery.Delete(category);
               
            }
        }



        void Category_OnNotifyPropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            gallery.Update((CategoryViewModel)sender);
        }



    }
}
