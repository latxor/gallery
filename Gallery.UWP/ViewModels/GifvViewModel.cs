﻿using Gallery.UWP.Data.Enum;
using Gallery.UWP.Data.Helper;
using Gallery.UWP.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.UWP.ViewModels
{
    public class GifvViewModel: NotificationBase<Gifv>
    {
        public GifvViewModel(Gifv gifv = null) : base(gifv)
        {

        }

        public int CategoryId
        {
            get { return This.CategoryId; }
            set { SetProperty(This.CategoryId, value, () => This.CategoryId = value); }
        }

        public ImageType Type { get { return ImageType.Gifv; } }

        public gifvitem gifvitem
        {
            get { return This.gifvitem; }
            set { SetProperty(This.gifvitem, value, () => This.gifvitem = value); }
        }

        #region Commons Properties

        public int Id
        {
            get { return This.Id; }
            set { SetProperty(This.Id, value, () => This.Id = value); }
        }

        public string Url
        {
            get { return This.Url; }
            set { SetProperty(This.Url, value, () => This.Url = value); }
        }


        public bool Enable
        {
            get { return This.Enable; }
            set { SetProperty(This.Enable, value, () => This.Enable = value); }
        }

        public string CleanUrl
        {
            get { return This.CleanUrl; }
            set { SetProperty(This.CleanUrl, value, () => This.CleanUrl = value); }
        }

        #endregion
    }
}
