﻿using Gallery.UWP.Data.Helper;
using Gallery.UWP.Data.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gallery.UWP.ViewModels
{
    public class CategoryViewModel: NotificationBase<Category>
    {
        Category category;
        ObservableCollection<GfycatViewModel> _GfyCats = new ObservableCollection<GfycatViewModel>();
        ObservableCollection<GifvViewModel> _Gifvs = new ObservableCollection<GifvViewModel>();
        ObservableCollection<GifViewModel> _Gif = new ObservableCollection<GifViewModel>();
        ObservableCollection<OtherImagesViewModel> _OtherImages = new ObservableCollection<OtherImagesViewModel>();


        public CategoryViewModel(Category category = null) : base(category)
        { }

        public int Id
        {
            get { return This.Id; }
            set { SetProperty(This.Id, value, () => This.Id = value); }
        }
        public String Name
        {
            get { return This.Name; }
            set { SetProperty(This.Name, value, () => This.Name = value); }
        }

        public bool Selected
        {
            get { return This.Selected; }
            set { SetProperty(This.Selected, value, () => This.Selected = value); }
        }

        public bool Enable
        {
            get { return This.Enable; }
            set { SetProperty(This.Enable, value, () => This.Enable = value); }
        }

        public String CleanName
        {
            get { return This.CleanName; }
            set { SetProperty(This.CleanName, value, () => This.CleanName = value); }
        }

        public int UserId
        {
            get { return This.UserId; }
            set { SetProperty(This.UserId, value, () => This.UserId = value); }
        }

        #region Codigo No Utilizado
        //public CategoryViewModel(string name)
        //{
        //    //category = new Category() { Name = name, Enable = true, CleanName = UrlFactory.RemoveSpecialCharacters(name) };
        //    //_SelectedIndex = -1;

        //    //foreach (var gfycat in category.GfyCat)
        //    //{
        //    //    var np = new GfycatViewModel(gfycat);
        //    //    np.PropertyChanged += Gfycat_OnNotifyPropertyChanged;
        //    //    _GfyCats.Add(np);
        //    //}

        //    //foreach (var gifv in category.Gifv)
        //    //{
        //    //    var np = new GifvViewModel(gifv);
        //    //    np.PropertyChanged += Gfycat_OnNotifyPropertyChanged;
        //    //    _Gifvs.Add(np);
        //    //}

        //    //foreach (var gif in category.Gif)
        //    //{
        //    //    var np = new GifViewModel(gif);
        //    //    np.PropertyChanged += Gif_OnNotifyPropertyChanged;
        //    //    _Gif.Add(np);
        //    //}

        //    //foreach (var otherimages in category.OthersImages)
        //    //{
        //    //    var np = new OtherImagesViewModel(otherimages);
        //    //    np.PropertyChanged += OtherImages_OnNotifyPropertyChanged;
        //    //    _OtherImages.Add(np);
        //    //}
        //}

        //int _SelectedIndex;
        //public int SelectedIndex
        //{
        //    get { return _SelectedIndex; }
        //    set
        //    {
        //        if (SetProperty(ref _SelectedIndex, value))
        //        { RaisePropertyChanged(nameof(SelectedGfycat)); }
        //    }
        //}

        //public GfycatViewModel SelectedGfycat
        //{
        //    get { return (_SelectedIndex >= 0) ? _GfyCats[_SelectedIndex] : null; }
        //}

        //void Gfycat_OnNotifyPropertyChanged(Object sender, PropertyChangedEventArgs e)
        //{
        //    category.UpdateGfyCat((GfycatViewModel)sender);
        //}

        //void Gifv_OnNotifyPropertyChanged(Object sender, PropertyChangedEventArgs e)
        //{
        //    category.UpdateGifv((GifvViewModel)sender);
        //}

        //void Gif_OnNotifyPropertyChanged(Object sender, PropertyChangedEventArgs e)
        //{
        //    category.UpdateGif((GifViewModel)sender);
        //}

        //void OtherImages_OnNotifyPropertyChanged(Object sender, PropertyChangedEventArgs e)
        //{
        //    category.UpdateOtherImages((OtherImagesViewModel)sender);
        //}

        #endregion
    }
}
